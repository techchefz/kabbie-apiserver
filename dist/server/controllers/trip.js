'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _APIError = require('../helpers/APIError');

var _APIError2 = _interopRequireDefault(_APIError);

var _transformResponse = require('../service/transform-response');

var _trip = require('../models/trip');

var _trip2 = _interopRequireDefault(_trip);

var _tripRequest = require('../models/trip-request');

var _tripRequest2 = _interopRequireDefault(_tripRequest);

var _scheduleTrip = require('../models/scheduleTrip');

var _scheduleTrip2 = _interopRequireDefault(_scheduleTrip);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Return the trip details of the user.
 * @param req
 * @param res
 * @param next
 * @returns { trip: historyObjArray[{ tripObj }]  }
 */

function getHistory(req, res, next) {
  var historyObjArray = [];
  var userID = req.user._id; //eslint-disable-line
  var userType = req.user.userType;
  var searchObj = {};
  if (userType === 'rider') {
    searchObj.riderId = userID;
  } else if (userType === 'driver') {
    searchObj.driverId = userID;
  }
  _trip2.default.find({ $and: [searchObj, { tripStatus: 'endTrip' }] }).sort({ bookingTime: -1 }).populate('riderId driverId').then(function (populatedTrips) {
    if (populatedTrips.length !== 0) {
      var returnObj = {
        success: true,
        message: 'user trip history ',
        data: populatedTrips
      };
      res.send(returnObj);
    } else {
      var _returnObj = {
        success: true,
        message: 'no history available',
        data: []
      };
      res.send(_returnObj);
    }
  });
}

function getAllScheduleTrips(req, res, next) {
  _scheduleTrip2.default.find({ riderId: req.headers.user_id }).sort({ scheduleOn: 1 }).populate('riderId').then(function (tripObj) {
    if (tripObj.length !== 0) {
      var returnObj = {
        success: true,
        message: 'user trip history',
        data: tripObj
      };
      res.send(returnObj);
    } else {
      var _returnObj2 = {
        success: false,
        message: 'no history available',
        data: []
      };
      res.send(_returnObj2);
    }
  });
}

function cancelScheduleTrip(req, res, next) {
  _scheduleTrip2.default.findOneAndUpdate({ _id: req.body.tripId }, { $set: { tripRequestStatus: req.body.changeTripStatus } }).then(function (cancelledTripObj) {
    var returnObj = {
      success: true,
      message: 'Found And Cancelled',
      data: cancelledTripObj
    };
    res.send(returnObj);
  });
}

function subTripAmount(req, res, next) {
  var NewTripAmount = 0;
  _tripRequest2.default.findOneAsync({ _id: req.body.tripObj._id }).then(function (foundTrip) {
    NewTripAmount = foundTrip.tripAmt + req.body.tripAmount;
    _tripRequest2.default.findOneAndUpdate({ _id: req.body.tripObj._id }, { $set: { tripAmt: NewTripAmount } }, { new: true }).then(function (newAmount) {
      var returnObj = {
        success: false,
        message: 'Not Inserted',
        data: []
      };
      if (!newAmount) {
        res.send(returnObj);
      } else {
        returnObj.success = true;
        returnObj.message = "Inserted Amnt";
        returnObj.data = newAmount;
        res.send(returnObj);
      }
    });
  });
}

function FinalTripAmount(req, res, next) {
  var FinalTripAmount = 0;
  // TripSchema.findOneAsync({ _id: req.body.tripObj._id })
  //   .then((foundTrip) => {
  //     console.log('===============foundTrip=====================');
  //     console.log(foundTrip);
  //     console.log(req.body.tripAmount);
  //     console.log('===============foundTrip=====================');
  FinalTripAmount = req.body.tripAmount + req.body.previousTripAmount;
  // console.log('====================================')
  // console.log(req.body.previousTripAmount)
  // console.log('====================================')
  // console.log(req.body.tripAmount)
  // console.log('====================================')
  // console.log(FinalTripAmount)
  // console.log('====================================')
  _trip2.default.findOneAndUpdate({ _id: req.body.tripObj._id }, { $set: { tripAmt: FinalTripAmount } }, { new: true }).then(function (finalAmount) {
    var returnObj = {
      success: false,
      message: 'Not Inserted',
      data: []
    };
    if (!finalAmount) {
      res.send(returnObj);
    } else {
      returnObj.success = true;
      returnObj.message = "Inserted Amnt";
      returnObj.data = finalAmount;
      res.send(returnObj);
    }
  });
}

exports.default = {
  getHistory: getHistory,
  getAllScheduleTrips: getAllScheduleTrips,
  cancelScheduleTrip: cancelScheduleTrip,
  subTripAmount: subTripAmount,
  FinalTripAmount: FinalTripAmount
};
module.exports = exports['default'];
//# sourceMappingURL=trip.js.map
