"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _selfRide = require("../models/self-ride");

var _selfRide2 = _interopRequireDefault(_selfRide);

var _user = require("../models/user");

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function testselfRide(req, res) {
    res.send("You Found Me :)");
}

function selfRideRequest(req, res) {
    var optValue = Math.floor(100000 + Math.random() * 900000); //eslint-disable-line

    var newRide = new _selfRide2.default({
        // riderId: { type: Schema.Types.ObjectId, ref: 'User' },
        driverId: req.body.driverId,
        riderName: req.body.riderName,
        riderPhoneNo: req.body.riderPhoneNo,
        pickUpAddress: req.body.pickUpAddress,
        destAddress: req.body.destAddress,
        bookTime: req.body.dateTime,
        carType: req.body.carType,
        amount: req.body.amount,
        // isTransfered: req.body.isTransfered,
        // srcLoc: { type: [Number], index: '2d' },
        // destLoc: { type: [Number], index: '2d' },
        tripOtp: optValue
        // paymentMode: req.body.paymentMode,
        // carType: req.body.carType
    });
    newRide.saveAsync().then(function (savedUser) {

        console.log('====================================');
        console.log(savedUser);
        console.log('====================================');
        if (savedUser) {
            var returnObj = {
                success: true,
                message: 'Successfully Saved',
                data: savedUser
            };
            res.send(returnObj);
        } else {
            var _returnObj = {
                success: false,
                message: 'Something Went Wrong!',
                data: null
            };
            res.send(_returnObj);
        }
    });
}

function transferRide(req, res) {
    var optValue = Math.floor(100000 + Math.random() * 900000); //eslint-disable-line


    _selfRide2.default.findOneAndUpdate({ _id: req.body.tripId }, { $set: { isTransfered: true } }).then(function (updatedTrip) {
        console.log('====================================');
        console.log(updatedTrip);
        console.log('====================================');
        var newRide = new _selfRide2.default({
            driverId: req.body.driverId,
            riderName: updatedTrip.riderName,
            riderPhoneNo: updatedTrip.riderPhoneNo,
            pickUpAddress: updatedTrip.pickUpAddress,
            destAddress: updatedTrip.destAddress,
            bookTime: updatedTrip.dateTime,
            // isTransfered: req.body.isTransfered,
            // srcLoc: { type: [Number], index: '2d' },
            // destLoc: { type: [Number], index: '2d' },
            tripOtp: optValue
            // paymentMode: req.body.paymentMode,
            // carType: req.body.carType
        });
        newRide.saveAsync().then(function (transferedRide) {
            if (transferedRide) {
                var returnObj = {
                    success: true,
                    message: 'Successfully Transfered',
                    data: transferedRide
                };
                res.send(returnObj);
            } else {
                var _returnObj2 = {
                    success: false,
                    message: 'Something Went Wrong!',
                    data: null
                };
                res.send(_returnObj2);
            }
        });
    });
}

function getAllSelfRide(req, res) {
    var returnObj = {
        success: false,
        message: 'Something Went Wrong! ,No Trips Found',
        data: []
    };
    console.log('====================================');
    console.log(req.headers);
    console.log('====================================');
    _selfRide2.default.find({ driverId: req.headers.user_id }).sort({ bookTime: 1 }).then(function (foundTrips) {
        console.log('====================================');
        console.log(foundTrips);
        console.log('====================================');
        if (foundTrips) {
            returnObj.success = true;
            returnObj.message = "All Found Trips";
            returnObj.data = foundTrips;
            res.send(returnObj);
        } else {
            res.send(returnObj);
        }
    });
}

function rideStatusUpdate(req, res) {
    var returnObj = {
        success: false,
        message: 'Status Updation failed',
        data: []
    };
    console.log('==========bodybodybody==========================');
    console.log(req.body);
    console.log('===========bodybodybody=========================');
    _selfRide2.default.findOneAndUpdate({ _id: req.body.data._id }, { $set: { tripRequestStatus: req.body.data.tripRequestStatus } }, { new: true }).then(function (updatedData) {
        _selfRide2.default.find({ driverId: updatedData.driverId }).sort({ bookTime: 1 }).then(function (foundTrips) {
            console.log('====================================');
            console.log(foundTrips);
            console.log('====================================');
            if (foundTrips) {
                returnObj.success = true;
                returnObj.message = "updated trips";
                returnObj.data = foundTrips;
                res.send(returnObj);
            } else {
                res.send(returnObj);
            }
        });
    });
}

function getAllDrivers(req, res) {
    var returnObj = {
        success: false,
        message: 'Something Went Wrong!, No Driver`s Found',
        data: []
    };
    var driverCarType = req.headers.cartype;
    var foundDriver = [];
    _user2.default.find({ userType: "driver" }).then(function (foundDrivers) {
        foundDrivers.forEach(function (drivers) {
            console.log('====================================');
            console.log(driverCarType);
            console.log(drivers.carDetails.type);
            console.log('====================================');
            if (drivers.carDetails.type == driverCarType) {
                console.log("chala");
                foundDriver.push(drivers);
            }
        });
    }).then(function () {
        returnObj.success = true;
        returnObj.message = "All Found Drivers";
        returnObj.data = foundDriver;
        res.send(returnObj);
    });
}

exports.default = {
    selfRideRequest: selfRideRequest,
    transferRide: transferRide,
    getAllSelfRide: getAllSelfRide,
    getAllDrivers: getAllDrivers,
    testselfRide: testselfRide,
    rideStatusUpdate: rideStatusUpdate
};
module.exports = exports["default"];
//# sourceMappingURL=self-ride.js.map
