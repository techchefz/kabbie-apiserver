'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _query = require('../models/query');

var _query2 = _interopRequireDefault(_query);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function newQuery(req, res, next) {
    console.log(req.body);
    var newQuery = new _query2.default({
        userId: req.body._id,
        queryMessage: req.body.message,
        querySubject: req.body.subject
    });
    newQuery.saveAsync();
    var returnObj = {
        success: true,
        message: 'Query Successfully Submitted',
        data: newQuery
    };
    res.send(returnObj);
}

function getAllQuery(req, res, next) {
    _query2.default.find({ queryStatus: req.headers.querystatus }).sort({ submittedOn: -1 }).populate('userId').then(function (foundQuery) {
        var returnObj = {
            success: false,
            message: 'No Query Found',
            data: []
        };
        if (foundQuery.length == 0) {
            res.send(returnObj);
        } else {
            returnObj.success = true;
            returnObj.message = "Found Query`s";
            returnObj.data = foundQuery;
            res.send(returnObj);
        }
    });
}

function getadminQuery(req, res, next) {
    _query2.default.find().sort({ submittedOn: -1 }).populate('userId').then(function (foundQuery) {
        var returnObj = {
            success: false,
            message: 'No Query Found',
            data: []
        };
        if (foundQuery.length == 0) {
            res.send(returnObj);
        } else {
            returnObj.success = true;
            returnObj.message = "Found Query`s";
            returnObj.data = foundQuery;
            res.send(returnObj);
        }
    });
}

function changeQueryStatus(req, res, next) {
    _query2.default.findOneAndUpdate({ _id: req.body.tripId }, { $set: { queryStatus: req.body.queryStatus } }, { new: true }).then(function (updateQuery) {
        var returnObj = {
            success: false,
            message: 'Not Found Or Something Went Wrong!',
            data: []
        };
        if (updateQuery) {
            returnObj.success = true;
            returnObj.message = "Stauts Changed";
            returnObj.data = updateQuery;
            res.send(returnObj);
        } else {
            res.send(returnObj);
        }
    });
}

exports.default = {
    newQuery: newQuery,
    getAllQuery: getAllQuery,
    changeQueryStatus: changeQueryStatus,
    getadminQuery: getadminQuery
};
module.exports = exports['default'];
//# sourceMappingURL=query.js.map
