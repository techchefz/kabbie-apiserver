'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var selfRide = new Schema({
    riderId: { type: Schema.Types.ObjectId, ref: 'User' },
    driverId: { type: Schema.Types.ObjectId, ref: 'User' },
    riderName: { type: String },
    riderPhoneNo: { type: String },
    pickUpAddress: { type: String, default: null },
    destAddress: { type: String, default: null },
    isTransfered: { type: Boolean, default: false },
    srcLoc: { type: [Number], index: '2d' },
    destLoc: { type: [Number], index: '2d' },
    tripOtp: { type: Number },
    paymentMode: { type: String, default: 'CASH' },
    carType: { type: String, default: 'Quickest' },
    tripRequestStatus: { type: String, default: 'pending' },
    latitudeDelta: { type: Number, default: 0.012 },
    longitudeDelta: { type: Number, default: 0.012 },
    requestTime: { type: Date, default: Date.now },
    bookTime: { type: Date, default: null },
    amount: { type: Number, default: 0 }
});

exports.default = _mongoose2.default.model('selfRide', selfRide);
module.exports = exports['default'];
//# sourceMappingURL=self-ride.js.map
