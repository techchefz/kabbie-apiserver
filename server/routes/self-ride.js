import express from 'express';
import httpStatus from 'http-status';
import passport from 'passport';

import APIError from '../helpers/APIError';
import config from '../../config/env';

import selfRideCtrl from "../controllers/self-ride";


const router = express.Router();


/** GET /api/self-ride/getAllSelfRide  */

router.route('/testSelfRide')
    .get(selfRideCtrl.testselfRide);


/**
* Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
*/
router.use((req, res, next) => {
    passport.authenticate('jwt', config.passportOptions, (error, userDtls, info) => { //eslint-disable-line
        if (error) {
            const err = new APIError('token not matched', httpStatus.INTERNAL_SERVER_ERROR);
            return next(err);
        } else if (userDtls) {
            req.user = userDtls;
            next();
        } else {
            const err = new APIError(`token is valid but no user found ${info}`, httpStatus.UNAUTHORIZED);
            return next(err);
        }
    })(req, res, next);
});



// /** POST /api/self-ride/selfRideRequest  */
router.route('/selfRideRequest')
    .post(selfRideCtrl.selfRideRequest);

/** GET /api/self-ride/getAllSelfRide  */

router.route('/getAllSelfRide')
    .get(selfRideCtrl.getAllSelfRide);


/** POST /api/self-ride/getAllSelfRide  */

router.route('/transferSelfRide')
    .post(selfRideCtrl.transferRide);

/** GET /api/self-ride/getAllSelfRide  */

router.route('/getAllDrivers')
    .get(selfRideCtrl.getAllDrivers);

/** GET /api/self-ride/rideStatusUpdate  */

router.route('/rideStatusUpdate')
    .post(selfRideCtrl.rideStatusUpdate);



export default router;