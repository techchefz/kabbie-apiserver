
import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const ScheduleTripSchema = new Schema({
    riderId: { type: Schema.Types.ObjectId, ref: 'User' },
    driverId: { type: Schema.Types.ObjectId, ref: 'User' },
    tripId: { type: Schema.Types.ObjectId, ref: 'trip' },
    srcLoc: {
        type: [Number],
        index: '2d'
    },
    destLoc: {
        type: [Number],
        index: '2d'
    },
    tripOtp: { type: Number },
    paymentMode: { type: String, default: 'CASH' },
    carType: { type: String, default: 'Economy' },
    tripRequestStatus: { type: String, default: 'request' },
    tripRequestIssue: { type: String, default: 'noIssues' },
    pickUpAddress: { type: String, default: null },
    destAddress: { type: String, default: null },
    latitudeDelta: { type: Number, default: 0.012 },
    longitudeDelta: { type: Number, default: 0.012 },
    scheduleOn: { type: Date, default: null },
    requestTime: { type: Date, default: Date.now }
});

export default mongoose.model('ScheduleTripSchema', ScheduleTripSchema);