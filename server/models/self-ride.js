
import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const selfRide = new Schema({
    riderId: { type: Schema.Types.ObjectId, ref: 'User' },
    driverId: { type: Schema.Types.ObjectId, ref: 'User' },
    riderName: { type: String },
    riderPhoneNo: { type: String },
    pickUpAddress: { type: String, default: null },
    destAddress: { type: String, default: null },
    isTransfered: { type: Boolean, default: false },
    srcLoc: { type: [Number], index: '2d' },
    destLoc: { type: [Number], index: '2d' },
    tripOtp: { type: Number },
    paymentMode: { type: String, default: 'CASH' },
    carType: { type: String, default: 'Quickest' },
    tripRequestStatus: { type: String, default: 'pending' },
    latitudeDelta: { type: Number, default: 0.012 },
    longitudeDelta: { type: Number, default: 0.012 },
    requestTime: { type: Date, default: Date.now },
    bookTime: { type: Date, default: null },
    amount: { type: Number, default: 0 }
});

export default mongoose.model('selfRide', selfRide);