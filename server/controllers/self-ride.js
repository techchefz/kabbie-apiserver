import SelfRideSchema from "../models/self-ride";
import UserSchema from "../models/user";

function testselfRide(req, res) {
    res.send("You Found Me :)")
}

function selfRideRequest(req, res) {
    const optValue = Math.floor(100000 + Math.random() * 900000); //eslint-disable-line

    const newRide = new SelfRideSchema({
        // riderId: { type: Schema.Types.ObjectId, ref: 'User' },
        driverId: req.body.driverId,
        riderName: req.body.riderName,
        riderPhoneNo: req.body.riderPhoneNo,
        pickUpAddress: req.body.pickUpAddress,
        destAddress: req.body.destAddress,
        bookTime: req.body.dateTime,
        carType: req.body.carType,
        amount: req.body.amount,
        // isTransfered: req.body.isTransfered,
        // srcLoc: { type: [Number], index: '2d' },
        // destLoc: { type: [Number], index: '2d' },
        tripOtp: optValue,
        // paymentMode: req.body.paymentMode,
        // carType: req.body.carType
    });
    newRide.saveAsync().then((savedUser) => {

        console.log('====================================')
        console.log(savedUser)
        console.log('====================================')
        if (savedUser) {
            const returnObj = {
                success: true,
                message: 'Successfully Saved',
                data: savedUser
            };
            res.send(returnObj);
        } else {
            const returnObj = {
                success: false,
                message: 'Something Went Wrong!',
                data: null
            };
            res.send(returnObj);
        }
    });
}

function transferRide(req, res) {
    const optValue = Math.floor(100000 + Math.random() * 900000); //eslint-disable-line


    SelfRideSchema.findOneAndUpdate({ _id: req.body.tripId }, { $set: { isTransfered: true } })
        .then((updatedTrip) => {
            console.log('====================================');
            console.log(updatedTrip);
            console.log('====================================');
            const newRide = new SelfRideSchema({
                driverId: req.body.driverId,
                riderName: updatedTrip.riderName,
                riderPhoneNo: updatedTrip.riderPhoneNo,
                pickUpAddress: updatedTrip.pickUpAddress,
                destAddress: updatedTrip.destAddress,
                bookTime: updatedTrip.dateTime,
                // isTransfered: req.body.isTransfered,
                // srcLoc: { type: [Number], index: '2d' },
                // destLoc: { type: [Number], index: '2d' },
                tripOtp: optValue,
                // paymentMode: req.body.paymentMode,
                // carType: req.body.carType
            });
            newRide.saveAsync().then((transferedRide) => {
                if (transferedRide) {
                    const returnObj = {
                        success: true,
                        message: 'Successfully Transfered',
                        data: transferedRide
                    };
                    res.send(returnObj);
                } else {
                    const returnObj = {
                        success: false,
                        message: 'Something Went Wrong!',
                        data: null
                    };
                    res.send(returnObj);
                }
            });
        })
}

function getAllSelfRide(req, res) {
    const returnObj = {
        success: false,
        message: 'Something Went Wrong! ,No Trips Found',
        data: []
    };
    console.log('====================================')
    console.log(req.headers)
    console.log('====================================')
    SelfRideSchema.find({ driverId: req.headers.user_id }).sort({ bookTime: 1 })
        .then((foundTrips) => {
            console.log('====================================');
            console.log(foundTrips);
            console.log('====================================');
            if (foundTrips) {
                returnObj.success = true;
                returnObj.message = "All Found Trips";
                returnObj.data = foundTrips;
                res.send(returnObj);
            } else {
                res.send(returnObj);
            }
        });
}

function rideStatusUpdate(req, res) {
    const returnObj = {
        success: false,
        message: 'Status Updation failed',
        data: []
    };
    console.log('==========bodybodybody==========================')
    console.log(req.body)
    console.log('===========bodybodybody=========================')
    SelfRideSchema.findOneAndUpdate({ _id: req.body.data._id }, { $set: { tripRequestStatus: req.body.data.tripRequestStatus } }, { new: true })
        .then((updatedData) => {
            SelfRideSchema.find({ driverId: updatedData.driverId }).sort({ bookTime: 1 })
                .then((foundTrips) => {
                    console.log('====================================');
                    console.log(foundTrips);
                    console.log('====================================');
                    if (foundTrips) {
                        returnObj.success = true;
                        returnObj.message = "updated trips";
                        returnObj.data = foundTrips;
                        res.send(returnObj);
                    } else {
                        res.send(returnObj);
                    }
                });
        })
}

function getAllDrivers(req, res) {
    const returnObj = {
        success: false,
        message: 'Something Went Wrong!, No Driver`s Found',
        data: []
    };
    let driverCarType = req.headers.cartype;
    let foundDriver = [];
    UserSchema.find({ userType: "driver" })
        .then((foundDrivers) => {
            foundDrivers.forEach((drivers) => {
                console.log('====================================');
                console.log(driverCarType);
                console.log(drivers.carDetails.type);
                console.log('====================================');
                if (drivers.carDetails.type == driverCarType) {
                    console.log("chala")
                    foundDriver.push(drivers);
                }
            })
        }).then(() => {
            returnObj.success = true;
            returnObj.message = "All Found Drivers";
            returnObj.data = foundDriver;
            res.send(returnObj);
        });
}

export default {
    selfRideRequest,
    transferRide,
    getAllSelfRide,
    getAllDrivers,
    testselfRide,
    rideStatusUpdate
}